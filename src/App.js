import OpenModal from "./container/Page";
import "./App.css";

function App() {
  return (
    <div>
      <OpenModal />
    </div>
  );
}

export default App;
