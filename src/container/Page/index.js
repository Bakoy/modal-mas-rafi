import React, { Component } from "react";
import "./style.css";
import Modal from "../../components/Modal";
import Content from "../../components/Content";

class OpenModal extends Component {
  state = {
    showModel: false,
  };

  handleModelOpen = () => {
    this.setState({ showModel: true });
  };

  handleModelClose = () => {
    this.setState({ showModel: false });
  };

  componentDidMount() {
    document.addEventListener("keyup", (e) => {
      if (e.key === "Escape") {
        this.handleModelClose();
      }
    });
    window.addEventListener("keyup", this.handleKeyUp, false);
    document.addEventListener("click", this.handleOutsideClick, false);
  }

  componentWillUnmount() {
    window.removeEventListener("keyup", this.handleKeyUp, false);
    document.removeEventListener("click", this.handleOutsideClick, false);
  }

  render() {
    return (
      <div>
        <div className="WrapperButton">
          <button
            className="ButtonOpen"
            style={{
              zIndex: !this.state.showModel ? "100" : "0",
            }}
            onClick={this.handleModelOpen}
          >
            Open Me
          </button>
        </div>
        <Modal show={this.state.showModel} modalClosed={this.handleModelClose}>
          <Content handleModelClose={this.handleModelClose} dataHtml={<p>coba kirim html</p>} />
        </Modal>
      </div>
    );
  }
}

export default OpenModal;
