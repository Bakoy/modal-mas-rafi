import React from "react";
import "./style.css";

const Content = ({ handleModelClose, dataHtml }) => {
  // console.log(classes);
  return (
    <div className="modal-content">
      <div className={`modal-header ModelHeader`}>
        <h4 className="H4">Modal Header</h4>
        <h2>
          <span className={` close`} onClick={handleModelClose}>
            &times;
          </span>
        </h2>
      </div>
      <div className={`modal-body ModelBody`}>
        <p>
          Lorem ipsum dolor sit amet consectetur, adipisicing elit. Omnis veritatis corrupti odio voluptate tempore ipsam, fugit, quod iure sed delectus qui sapiente incidunt cumque, nulla ullam exercitationem id ea enim. Lorem ipsum dolor
          sit amet consectetur adipisicing elit. Blanditiis velit, maxime consequuntur saepe in, enim ad sit dolorum voluptas, asperiores totam. Corporis ex eveniet perspiciatis quaerat expedita, harum perferendis minima?
        </p>
      </div>
      <div className={`modal-footer ModelFooter`}>
        <h3>Modal Footer</h3>
        <button className="CloseButton" onClick={handleModelClose}>
          Close
        </button>
      </div>
      {dataHtml}
    </div>
  );
};
export default Content;
